# Sabaidea memory leak detection challenge
Finished project source code located **[here](https://gitlab.com/mohamad_koohkan/memory-leak-challenge/-/blob/master/MemoryLeakDetectionChallenge/MemoryLeakDetectionChallenge/MyViewController.swift)**.

### AnotherView delegate memory leak 

As we are using a class which is a refrence type we should mark delegate method with `weak` to avoid making strong reference cycles But what is a strong refrence cycle? whenever two class instances (in this case, instance of `AnotherView` and `MyViewController`), have **strong** refrence to each other their reference counts never go to zero so they never get deallocated. If we were in a struct or enum which is a value type they couldn't make strong refrence cycle's.

### How to solve the problem by code?

As we need to make a weak refrence instead of strong refrence cycle we need to make class-bound protocols

- Chaning `AnotherViewDelegate` protocol definition to be compatible with classes
```swift
protocol AnotherViewDelegate: class  
```

- Changing strong refrence to weak refrence
```swift
weak var delegate: AnotherViewDelegate?
```

### showAlert() actions 

The `showAlert()` method [here]("https://gitlab.com/mohamad_koohkan/memory-leak-challenge/-/blob/6fcec601e81a8715c6cf0d5652c11905b9568d91/Swift-Interview-Question.swift#L19") increases actions based on how many times this method calls.

Also defining [alert](https://gitlab.com/mohamad_koohkan/memory-leak-challenge/-/blob/6fcec601e81a8715c6cf0d5652c11905b9568d91/Swift-Interview-Question.swift#L5) as property of class makes a strong refrence between UIViewController and UIAlertController, This is why adding action to `alert` will remains, object will deallocate when MyViewController deallocated.

To solve this I tried to generate alert through a method for the class.

You can find this method code [here](https://gitlab.com/mohamad_koohkan/memory-leak-challenge/-/blob/development/MemoryLeakDetectionChallenge/MemoryLeakDetectionChallenge/MyViewController.swift#L79)



## Changelog
You can find list of changelogs **[here](https://gitlab.com/mohamad_koohkan/memory-leak-challenge/-/blob/master/MemoryLeakDetectionChallenge/CHANGELOG.md)**.