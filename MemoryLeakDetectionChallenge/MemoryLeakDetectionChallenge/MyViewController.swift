//
//  ViewController.swift
//  MemoryLeakDetectionChallenge
//
//  Created by Mohammad reza Koohkan on 2/19/1399 AP.
//  Copyright © 1399 AP Mohamadreza Koohkan. All rights reserved.
//

import UIKit

/// Marking `MyViewController` with `final` tells Swift compiler to make a `static dispatch` instead of
/// `dynamic dispatch` this reduces function call overhead and gives us extra performance
///
final class MyViewController: UIViewController {
    
    /// `anotherView` should be a property of the class and because of `lazy loading` the property have
    /// initial value of `nil`, so we can initialize from `__lazy_storage` whenever we needed
    ///
    /// If anotherView can be accessed by multiple threads before it is initialized, there is a chance of
    /// initializing more than once if several threads access the view at the same time, In that case it
    /// is better to go forward for a **let** initalized anotherView
    ///
    final private lazy var anotherView: AnotherView = { [unowned self] in
        let view = AnotherView()
        return view
        }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupAnotherView()
    }
    
    
    /// Initialze `anotherView`, sets delegate and setup `anotherView` if view loaded
    ///
    final private func setupAnotherView() {
        self.anotherView.delegate = .init(didTapOnButton: { [weak self] btn in
            self?.didTapOnButton(btn)
        })
        
        guard self.isViewLoaded else {
            // we can log related issues
            fatalError("Adding another view while view is not loaded")
        }
        self.view.addSubview(self.anotherView)
        self.anotherView.fillSuperView()
    }
    
    
    /// Tells `AnotherViewDelegate` that button iside `anotherView` being tapped through closure
    ///
    func didTapOnButton(_ sender: UIButton) {
        print("Received AnotherView event from AnotherViewDelegate.didTapOnButton")
        self.showAlert()
    }
    
    /// Shows alert and ARC takes care of `alert` deallocation
    ///
    final private func showAlert() {
        let alert = self.createAlert()
        alert.addAction(self.deleteAction())
        alert.addAction(self.cancelAction())
        self.present(alert, animated: true)
    }
    
    /// As `UIAlertController` is a Refrence Type, **alert** needed to
    /// intialize every time with newActions, so we created a method to generate
    /// new one everytime, as `UIAlertController` is kind of `UIViewController` it will dealocate
    /// when user dismiss the alert, deallocation goes over `__NSMallocBlock__` (malloc block)
    /// while object just allocated in one copy statically in heap
    ///
    /// **For best experience these services should store separately for further usage**
    ///
    /// Previous alert problems:
    /// - Had strong refrence cycle
    /// - Stores new actions everytime
    /// - ARC takes care of allocated memory
    ///
    final private func createAlert() -> UIAlertController {
        let title = "Alert Title"
        let message = "Alert Message"
        let style = UIAlertController.Style.actionSheet
        return UIAlertController(title: title, message: message, preferredStyle: style)
    }
    
    /// Returns a delete action
    ///
    /// Manages calling to it's `handler` if `self` was available
    ///
    final private func deleteAction() -> UIAlertAction {
        return UIAlertAction(title: "Delete", style: .default) { [unowned self] _ in
            self.deleteStuff()
        }
    }
    
    /// Returns a cancel action
    ///
    final private func cancelAction() -> UIAlertAction {
        UIAlertAction(title: "Cancel", style: .destructive)
    }
    
    final private func deleteStuff() {
        print("Delete Item")
    }
}

/// AnotherViewDelegate using 
///
final class AnotherViewDelegate {
    
    var didTapOnButton: (UIButton) -> ()
    
    func button(didTapped btn: UIButton) {
        self.didTapOnButton(btn)
    }
    
    init(didTapOnButton: @escaping (UIButton) -> ()) {
        self.didTapOnButton = didTapOnButton
    }
    
}

final class AnotherView: UIView {
    
    
    /// Closure delegation
    ///
    var delegate: AnotherViewDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    /// Inside Swift stdlib there is no force to set delegate on object initialization, so we moved
    /// delegation out of initialization scope
    ///
    init() {
        super.init(frame: .zero)
        self.commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    private func commonInit() {
        let button = UIButton(frame: frame)
        button.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        button.addTarget(self, action: #selector(buttonTapped(_:)), for: .touchUpInside)
        self.addSubview(button)
    }
    
    @objc private func buttonTapped(_ sender: UIButton) {
        self.delegate?.button(didTapped: sender)
    }
    
    func fillSuperView() {
        guard let parent = self.superview else { return }
        self.translatesAutoresizingMaskIntoConstraints = false
        self.topAnchor.constraint(equalTo: parent.topAnchor).isActive = true
        self.bottomAnchor.constraint(equalTo: parent.bottomAnchor).isActive = true
        self.leadingAnchor.constraint(equalTo: parent.leadingAnchor).isActive = true
        self.trailingAnchor.constraint(equalTo: parent.trailingAnchor).isActive = true
    }
}
