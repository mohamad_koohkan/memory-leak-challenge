## Sabaidea memory leak detection challenge changelogs

List of changelog related to source on **["master"](https://gitlab.com/mohamad_koohkan/memory-leak-challenge/-/blob/master/MemoryLeakDetectionChallenge/MemoryLeakDetectionChallenge/MyViewController.swift)**

## MyViewController dispatch changes
Marking `MyViewController` with `final` tells Swift compiler to make a `static dispatch` instead of `dynamic dispatch`

- Marking with `final`
  - This reduces function call overhead and gives us extra performance.
```swift
final class MyViewController: UIViewController, AnotherViewDelegate
```

## AnotherViewDelegate class-bound protocol

As we need to make a weak refrence instead of strong refrence cycle we need to make class-bound protocols

- Change non-class-bound protocol definition to class-bound protocol
```swift
protocol AnotherViewDelegate: class  
```

## AnotherView delegate memory leak 

As we are using a class which is a refrence type we should mark delegate method with `weak` to avoid making strong reference cycles But what is a strong refrence cycle? whenever two class instances (in this case, instance of `AnotherView` and `MyViewController`), have **strong** refrence to each other their reference counts never go to zero so they never get deallocated. If we were in a struct or enum which is a value type they couldn't make strong refrence cycle's.

- Changing strong refrence to weak refrence
```swift
weak var delegate: AnotherViewDelegate?
```
    

## AnotherView instance with lazy loading

If anotherView can be accessed by multiple threads before it is initialized, there is a chance of initializing more than once if several threads access the view at the same time, In that case it is better to go forward for a **let** initalized anotherView
    
- Lazy variables
  - `anotherView` should be a property of the class and because of `lazy loading` the property have initial value of `nil`
  - We can initialize from `__lazy_storage` whenever we needed
```swift
final private lazy var anotherView: AnotherView = { ... }()
```

- Setup after `viewLoaded` 
  - Initialze `anotherView`
  - Sets delegate and setup `anotherView` if view loaded
```swift
final private func setupAnotherView() 
```

- Pass data using `Closure`
```swift
final private func didTap(on button: UIButton) 
```

## UIAlertController property changed with a UIAlertController model generator 

As `UIAlertController` is a Refrence Type, **alert** needed to intialize every time with newActions, so we created a method to generate new one everytime, as `UIAlertController` is kind of `UIViewController` it will dealocate when user dismiss the alert, deallocation goes over `__NSMallocBlock__` (malloc block) while object just allocated in one copy statically in heap

- Create a `UIAlertController`
  - Had strong refrence cycle, changed to weak refrence
  - Stores new actions everytime
  - Now ARC takes care of allocated memory
```swift
final private func createAlert() -> UIAlertController 
```

- Delete action
  - Manages calling to it's `handler` if `self` was available
```swift
final private func deleteAction() -> UIAlertAction 
```

- Cancel action
```swift
final private func cancelAction() -> UIAlertAction 
```

## AnotherViewDelegate using closure

This feature trasfers `touchUpInside` event between sender and receiver through closures

- AnotherViewDelegate using closure
```swift
final class AnotherViewDelegate {
    
    var didTapOnButton: (UIButton) -> ()
    
    func button(didTapped btn: UIButton) {
        self.didTapOnButton(btn)
    }
    
    init(didTapOnButton: @escaping (UIButton) -> ()) {
        self.didTapOnButton = didTapOnButton
    }
    
}
```

- Usage at AnotherView
```swift
var delegate: AnotherViewDelegate?

@objc private func buttonTapped(_ sender: UIButton) {
  self.delegate?.button(didTapped: sender)
}
```
