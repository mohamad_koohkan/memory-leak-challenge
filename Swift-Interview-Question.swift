import UIKit

class MyViewController: UIViewController, AnotherViewDelegate {

	private let alert = UIAlertController(title: "Alert Title", message: "Alert Message", preferredStyle: .actionSheet)

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let anotherView = AnotherView(delegate: self)
        anotherView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(anotherView)
    }
    
    private func didTapOnButton(_ sender: UIButton) {
        showAlert()
    }
    
    private func showAlert() {

        let deleteAction = UIAlertAction(title: "Delete", style: .default, handler: { _ in
            self.deleteStuff()
        })
        
        alert.addAction(deleteAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
    }
    
    private func deleteStuff() {
        print("Delete Item")
    }
}


protocol AnotherViewDelegate {
    func didTapOnButton(_ sender: UIButton)
}

class AnotherView: UIView {
    
    var delegate: AnotherViewDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    init(delegate: AnotherViewDelegate) {
        super.init(frame: .zero)
        self.delegate = delegate
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit() {
        let button = UIButton(frame: frame)
        button.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        button.addTarget(self, action: #selector(buttonTapped(_:)), for: .touchUpInside)
        addSubview(button)
    }
    
    @objc private func buttonTapped(_ sender: UIButton) {
        delegate?.didTapOnButton(sender)
    }
}
