
### خطا
یک ایراد در رابطه با هدررفت حافظه (Memory Leak) در این کد وجود دارد. آن را بیابید و مشکلش را رفع کنید؛ سپس توضیح دهید چرا این مشکل به وجود آمده و راه حل شما چگونه به رفع آن کمک می‌کند.

### [محل خطا](https://gitlab.com/mohamad_koohkan/memory-leak-challenge/-/blob/6fcec601e81a8715c6cf0d5652c11905b9568d91/Swift-Interview-Question.swift#L45)

به دلیل اینکه هردو تایپ ما از جنس کلاس هستند و این دو کلاس 

- MyViewController
- AnotherView

و نشانه گری که به یک دیگر اشاره کنند دارند در صورتی که این نشانه گر از جنس 

weak

تعریف نشوند به طور پیش فرض از جنس 

strong

خواهند بود


```swift
var delegate: AnotherViewDelegate?
```

```swift
protocol AnotherViewDelegate {
    func didTapOnButton(_ sender: UIButton)
}
```

### راه حل
برای برطرف سازی مشکل ضعف حافظه باید این اینترفیس بین دو کلاس از جنس ویک تعریف شود
و پروتوکلی که این انترفیس را ساخته است باید قابل تطبیق با جنس کلاس ها باشد
بنابر این خواهیم داشت

```swift
weak var delegate: AnotherViewDelegate?
```

```swift
protocol AnotherViewDelegate: class {
    func didTapOnButton(_ sender: UIButton)
}
```


## تغییرات مورد نیاز
یک مورد از ساختار Delegate در این کد به کار رفته است. آن را حذف و از Closure استفاده کنید.

```swift
final class AnotherViewDelegate {
    
    var didTapOnButton: (UIButton) -> ()
    
    func button(didTapped btn: UIButton) {
        self.didTapOnButton(btn)
    }
    
    init(didTapOnButton: @escaping (UIButton) -> ()) {
        self.didTapOnButton = didTapOnButton
    }
    
}
```

```swift
self.anotherView.delegate = .init(didTapOnButton: { [weak self] btn in
    self?.didTapOnButton(btn)
})
```


